FROM golang:1.7.1-alpine

ENV GOPATH /go
ENV TARGET $GOPATH/src/github.com/spf13/hugo
RUN apk update && apk upgrade && \
    apk add --no-cache git openssh

RUN go get -u github.com/spf13/hugo && \
	git -C $TARGET remote add fork https://github.com/geeks-dev/hugo.git && \
	git -C $TARGET fetch fork && \
	git -C $TARGET checkout -b fork fork/fork && \
	go build -o $GOPATH/bin/hugo $TARGET/main.go && \
	rm -rf $GOPATH/src $GOPATH/pkg

RUN mkdir /usr/share/blog

WORKDIR /usr/share/blog


# ONBUILD ADD site/ /usr/share/blog
# ONBUILD RUN hugo -d /usr/share/nginx/html/
